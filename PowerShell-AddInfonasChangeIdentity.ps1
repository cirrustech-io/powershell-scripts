<#
Add infonas.com as new SMTP Address for all mailboxes
Change infonas.com to the primary identity for UserMailbox
#>

if (!$args[0]) {
  
}

$users= Get-Content $args[0]    

foreach($user in $users){

  $dt = Get-Date
  write-host '+ Processing User: ' $user ', ' $dt

  $user_name = $user.split('@')[0]
  $new_email_address = $user_name+'@infonas.com'
  
  $thisUser = Get-Recipient $user_name

  if ( $thisUser.RecipientType -Match "MailUniversalDistributionGroup" )
  {
    write-host '|--+ Type: DistributionGroup'
    Set-DistributionGroup $user_name -EmailAddresses @{add=$new_email_address}
    write-host '|  |-- Added new smtp address' $new_email_address 'for' $user_name
    
    Set-DistributionGroup $user_name -WindowsEmailAddress $new_email_address
    write-host '|  |-- Changed WindowsEmailAddress to: ' $new_email_address
   
  }


  if ( $thisUser.RecipientType -Match "UserMailbox" )
  {
    write-host '|--+ Type: UserMailbox'
    
    $thisMailbox = Get-Mailbox $user_name 
    
    Set-Mailbox $user_name -EmailAddresses @{add=$new_email_address}
    write-host '|  |-- Added new SMTP address' $new_email_address
    
    Set-Mailbox $user_name -WindowsEmailAddress $new_email_address
    write-host '|  |-- Swapped WindowsEmailAddress to' $new_email_address
    
    if ( $thisMailbox.IsShared -Match "False" )
    {
      write-host '|  |-- IsShared: No'
      $thisUser = Get-MsolUser -UserPrincipalName $user
      Set-MsolUserPrincipalName -NewUserPrincipalName $new_email_address -ObjectId $thisUser.ObjectId
      write-host '|  |-- User Mailbox Principal Name changed to: ' $new_email_address
    }
    else
    {
      write-host '|  |-- Shared Mailbox - No action' $new_email_address 'for' $user_name
    }
  
  }
  
  write-host '|  |-- Completed: ' $user
  
}

write-host '|+ Done'